﻿/*
* Thanks to Carlos Delgado for this post
* https://ourcodeworld.com/articles/read/128/how-to-play-pause-music-or-go-to-next-and-previous-track-from-windows-using-c-valid-for-all-windows-music-players
*/

using System;
using System.Runtime.InteropServices;
using Harmony;
using System.Reflection;

namespace Media_Controls
{
    public enum MediaKey { PlayPause, NextTrack, PreviousTrack };
    public class MediaControls : VTOLMOD
    {
        public const int KEYEVENTF_EXTENTEDKEY = 1;
        public const int KEYEVENTF_KEYUP = 0;
        public const int VK_MEDIA_NEXT_TRACK = 0xB0;// code to jump to next track
        public const int VK_MEDIA_PLAY_PAUSE = 0xB3;// code to play or pause a song
        public const int VK_MEDIA_PREV_TRACK = 0xB1;// code to jump to prev track

        private const string _harmonyID = "marsh.mediacontrols";

        private static MediaControls _instance;

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);

        public override void ModLoaded()
        {
            _instance = this;
            HarmonyInstance instance = HarmonyInstance.Create(_harmonyID);
            instance.PatchAll(Assembly.GetExecutingAssembly());
            base.ModLoaded();
        }

        public static void PressButton(MediaKey key)
        {
            _instance.Press(key);
        }

        private void Press(MediaKey key)
        {
            switch (key)
            {
                case MediaKey.PlayPause:
                    Log("Play/Pause Pressed");
                    keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
                    break;
                case MediaKey.NextTrack:
                    Log("Next Track Pressed");
                    keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
                    break;
                case MediaKey.PreviousTrack:
                    Log("Previous Track Pressed");
                    keybd_event(VK_MEDIA_PREV_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
                    break;
            }
        }
    }
}
