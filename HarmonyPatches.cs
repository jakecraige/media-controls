﻿using Harmony;

namespace Media_Controls
{
    [HarmonyPatch(typeof(CockpitRadio))]
    [HarmonyPatch("PlayButton")]
    public class Patch0
    {
        public static bool Prefix()
        {
            MediaControls.PressButton(MediaKey.PlayPause);
            return false;
        }
    }

    [HarmonyPatch(typeof(CockpitRadio))]
    [HarmonyPatch("NextSong")]
    public class Patch1
    {
        public static bool Prefix()
        {
            MediaControls.PressButton(MediaKey.NextTrack);
            return false;
        }
    }

    [HarmonyPatch(typeof(CockpitRadio))]
    [HarmonyPatch("PrevSong")]
    public class Patch2
    {
        public static bool Prefix()
        {
            MediaControls.PressButton(MediaKey.PreviousTrack);
            return false;
        }
    }
}
